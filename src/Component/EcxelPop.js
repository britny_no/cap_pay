
import React, { Component } from "react";

class EcxelPop extends Component {

  constructor(props) {
    super(props);
    this.state = {
        data: {},
        evidence_pop: {
          data: {},
          showing_key: null,
          showing: false
        }
    };
  }
  
  showEvidence = (evidence_pop, key) => {
    let state = this.state
    this.setState({
      data: state.data,
      evidence_pop: {
        data: evidence_pop,
        showing_key: key,
        showing: !state.evidence_pop.showing
      }
    }, () => {
      console.log(this.state)
    })
  }

  getDay = (day) =>{
    day = day.replace(/[^0-9]/gi, "@")
    let arr = day.split("@"), res = null

      if(day.charAt(day.length-1)=== "@"){
        res = Number(arr[arr.length-2])
      } else {
        res = Number(arr[arr.length-1])

      }
      return   res
  }

    render(){
     

      const div_style = {
        border: "1px solid black",
        width: "90vw",
        maxWidth: "480px",
        overflow: "scroll",
        height: "100%",
        position: "absolute",
        top: "10px",
        background: "white"

      }
      const data_style = {
        marginBottom : "10px",
        textAlign: "center",
        margin: "10px auto",
        width: "80%"
      }


      const span_style = {
        fontSize: "0.8em"
      }

      
     
      let ob_data = this.props.data, data_len = Object.keys(ob_data).length;
      let array = {};
      Object.keys(ob_data).map(k => {
              
        let data = ob_data[k];
        let date = data.date, price = data.price;
        let ob_date = this.getDay(date)
        let x = 0
        while(array[ob_date+x]){
            x = x+0.01
        }
        array[ob_date+x] = {
            date: date,
            price: price
        }
      })
      
        return(
          <div style ={div_style}>
              <br/>
          {   data_len === 0 ? "등록된 정보가 없습니다" :
              Object.keys(array).sort((a,b) => {return a-b;}).map(k => {
                let data = array[k];
                let  date = data.date, price = data.price;

                return <div key={k} style={data_style}>
                <span style = {span_style}>날짜: {date}</span>
                  <span>ㅤ</span>
                  <span style = {span_style}>금액: {price}</span>
                  <span>ㅤ</span>
                  <br/>
                
                </div>
              })
          }

       
        </div>
        );
    }
}




export default EcxelPop;
