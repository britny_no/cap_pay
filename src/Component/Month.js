
import React, { Component } from "react";
import Link from 'next/link';



class Month extends Component {

  constructor(props) {
    super(props);
    this.state = {
        data: {}
    };
  }
  

    render(){
      const max_style = {
        background: "white",
      margin: "0 auto" ,
      padding: "10px",
      maxWidth : "500px",
      height: "100vh"
      }


      

      let data = this.props.month_data;
      console.log(data)
        return(
          <div style={max_style}>
              {
              Object.keys(data).map(v => {
              let year = v.split("_")[0],  month = v.split("_")[1].split(".")[0];
              let text = year+"년 "+month +"월"
              
              return <div key = {v}>  <a>{text}</a></div>;
             })
             } 
        </div>
        );
    }
}




export default Month;
