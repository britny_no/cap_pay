
import React, { Component } from "react";
import axios from "axios";
import css from "../css/monthpop.module.css"
import { alertMove } from "../js";




class MonthPop extends Component {

  constructor(props) {
    super(props);
    this.state = {
        data: {},
        evidence_pop: {
          data: {},
          showing_key: null,
          showing: false
        }
    };
  }
  
  showEvidence = (evidence_pop, key) => {
    let state = this.state
    this.setState({
      data: state.data,
      evidence_pop: {
        data: evidence_pop,
        showing_key: key,
        showing: !state.evidence_pop.showing
      }
    })
  }

  deleteEvidence = (params) => {

    let data = {
      "file":  params[0].split("/")[2] ,
      "src": params[0],
      "price": params[1],
      "date": params[2],
      "user": params[3]
    }
    let session = this.props.session

    if( confirm("삭제하시겠습니까?")){
      
    if(session.login_id === ""){
      alertMove("로그인 이후에 이용해주세요", () => {
        location.href = "/"
      } )
      return
    } else if (data.user !== session.login_id) {
      alert("등록하신 사용자가 아니십니다")
      return
    }


    axios
    .post("/delete_contents", JSON.stringify(data), {
      headers: {
        "Content-Type": `application/json`,
      }
    })
    .then((res) => {
      let response = res.data
      console.log(response)
      if(response.state === 0){
        alertMove("로그인 이후에 이용해주세요", () => {
          location.href = "/"
        } )
      } else if (response.data === -1) {
        alert("에러가 발생했습니다. 다시 시도해주세요")
      } else {
        alertMove("성공", () => {
          location.href = "/MonthAll"
        } )
      }
    })
     
    }
  }

    render(){
     

      const div_style = {
        border: "1px solid black",
        width: "90vw",
        maxWidth: "480px",
        overflow: "scroll",
        height: "100%",
        position: "absolute",
        top: "10px",
        background: "white"

      }
      const data_style = {
        marginBottom : "10px",
        textAlign: "center",
        margin: "10px auto",
        width: "80%"
      }


      const span_style = {
        fontSize: "0.8em"
      }

      const button_Style = {
        fontSize: "0.9em",
        marginLeft: "5px",
        background: "#0000001a",
        borderRadius: "10px",
        width: "6em",
        height: "1.5em"
      }

      const delete_button_Style = {
        fontSize: "0.9em",
        marginLeft: "20px",
        background: "rgb(239 93 126 / 43%)",
        borderRadius: "10px",
        width: "6em",
        height: "1.5em"
      }
      
      const explain_style = {
        marginRight: "85px"
      }

      
     
      let ob_data = this.props.data, data_len = Object.keys(ob_data).length;
      let state = this.state;
      
        return(
          <div style ={div_style}>
              <br/>
          {   data_len === 0 ? "등록된 정보가 없습니다" :
              Object.keys(ob_data).map(k => {
              
                let data = ob_data[k];
                let src = data.src, date = data.date, price = data.price, error = data.error, user = data.user, is_delete = data.delete;
        
                return <div key={k} style={data_style}>
                  <img width = "100%" src={src}/>
                  <span style = {span_style}>금액: {price}</span>
                  <span>ㅤ</span>
                  <span style = {span_style}>날짜: {date}</span>
                  <span>ㅤ</span>
                  <span style = {span_style} >에러:ㅤ{error ? "o" : "x"}</span>
                  <br/>
                  <button style = {button_Style} onClick = {() => {this.showEvidence(data.evidence_src, k)}} >증거보기</button>
                  {is_delete === 1 ?  <button style = {delete_button_Style} onClick = {() => {this.deleteEvidence([src, price, date, user])}} >삭제하기</button>: null}
                  <br/>
                  <span style= {explain_style}className={css.evidence_close_span}>(한번더 클릭시, 닫아짐)</span>
                  {/* EvidencePop */}
                  {state.evidence_pop.showing &&  state.evidence_pop.showing_key === k ? <EvidencePop data={this.state.evidence_pop.data} /> : null }
                  <br/> <br/> <br/>
                </div>
              })
          }

       
        </div>
        );
    }
}

class EvidencePop extends Component {

 
  render() {
   
    let data = this.props.data.split(",")
 
    return (
    <div className={css.evidence_div}>{
      data.filter(v => v.replace(/ /gi, "") !== "").map( v => {
        return <div key = {v}>
                <span className={css.evidence_span}>증거</span>
                <img src={v}/>
                <br/>
                <br/>
                <br/>
              </div>
      })
    }</div>
    );
  }
}




export default MonthPop;
