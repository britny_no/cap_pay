
import React, { Component } from "react";
import Link from 'next/link';

class MovePage extends Component {


   
    render(){
      
  
      const max_style = {
        background: "white",
      margin: "0 auto" ,
      padding: "10px",
      maxWidth : "500px",
   
      }
      const h1_style = {
        fontSize: "1.5em"
      }

      const span_style = {
        fontSize: "0.8em",
        marginBottom: "10px"
      }

      const button_style = {
        background:" #00000014",
        borderRadius: "100px",
        width: "7em",
        height: "2em",
        marginBottom: "10px",
      
      }


      

     
        return(
          <nav style={max_style}>
            <h1 style = {h1_style}>Welcome!</h1>
            <br/>
            <span>  <Link key={1} href={{pathname:'/Register'}} ><button style= {button_style}>영수증 등록</button></Link></span>
          
            <br/>
            <span>   <Link key={2} href={{pathname:'/MonthAll'}} ><button  style= {button_style}>달별 모음</button></Link></span>

            <br/>
            <span>   <Link key={3} href={{pathname:'/Ecxel'}} ><button  style= {button_style}>엑셀 모음</button></Link></span>
            

            <br/>  <br/>
            <span>설명:</span> 
            <br/>
            <span style = {span_style}>영수증 등록 => 이달 사용한 영수증 등록(미리 입력된 가격, 날짜가 다를경우 수정 혹은 추가)</span> 
            <br/>
            <span  style = {span_style}>달별 모음 => 등록한 내용들 월별 정리</span> 
         
        </nav>
        );
    }
}




export default MovePage;
