var alertMove = (text, callback = () => {}) => {
  alert(text)
  callback()
}


var checkDate = (object) => {
  //날짜 정규표현식
  let result = 0
  
  let date = new Date(), now_year = date.getFullYear(), now_month = date.getMonth()+1;
  let split_year = now_year.toString().substr(2,2)

  //형식
  let regexp_form = [
    new RegExp('('+now_year+'\\.((([0-3]| |)[0-9])).((([0-3]| |)[0-9])))'), // 2020.05.01 or 2020.5.01
    new RegExp('('+now_year+' ((([0-3]| |)[0-9])) ((([0-3]| |)[0-9])))'), // 2020 05 01 or 2020 5 01
    new RegExp('('+now_year+'년,((([0-3]| |)[0-9])),(((([0-3]| |)[0-9])일)))'), // 2020년,5월,1일 or 2020년,05월,1일
    new RegExp('('+now_year+'년 ((([0-3]| |)[0-9]))월 (((([0-3]| |)[0-9])일)))'), // 2020년 5월 1일 or 2020년 05월 01일
    new RegExp('('+now_year+'년((([0-3]| |)[0-9]))월(((([0-3]| |)[0-9])일)))'), // 2020년5월1일 or 2020년05월1일
    new RegExp('('+now_year+'-((([0-3]| |)[0-9]))-((([0-3]| |)[0-9])))'), // 2020-5-1 or 2020-05-01
    new RegExp('('+now_year+'/((([0-3]| |)[0-9]))/((([0-3]| |)[0-9])))'), // 2020/5/1 or 2020/05/01
    new RegExp("(((([0-3]| |)[0-9]))/((([0-3]| |)[0-9]))\\([월화수목금토일]\\))"),// 5/27(수) or 05/27(수)
    new RegExp("(((([0-3]| |)[0-9]))\\.((([0-3]| |)[0-9]))\\([월화수목금토일]\\))"),// 5.27(수) or 05.27(수)
    new RegExp('(판매일:( |)'+split_year+'-((([0-3]| |)[0-9]))-((([0-3]| |)[0-9])))'), //판매일: 20-07-14
    new RegExp('(주문일시: '+now_year+'년 ((([0-3]| |)[0-9]))월 (((([0-3]| |)[0-9])일)))')//(주문일시: 2020년 8월 8일)
  ];

  // 형식+날짜
  let regexp = [
      new RegExp("("+now_year+'\\.((|0| )'+now_month+").(([0-3]| |)[0-9]))"), // 2020.05.01 or 2020.5.01
      new RegExp("("+now_year+' ((|0| )'+now_month+") (([0-3]| |)[0-9]))"), // 2020 05 01 or 2020 5 01
      new RegExp("("+now_year+'년,((|0| )'+now_month+"월)"+",(([0-3]| |)[0-9])일)"), // 2020년,5월,1일 or 2020년,05월,1일
      new RegExp("("+now_year+'년 ((|0| )'+now_month+"월)"+" (([0-3]| |)[0-9])일)"), // 2020년 5월 1일 or 2020년 05월 01일
      new RegExp("("+now_year+'년((|0| )'+now_month+"월)"+"(([0-3]| |)[0-9])일)"), // 2020년5월1일 or 2020년05월1일
      new RegExp("("+now_year+'-((|0| )'+now_month+")-(([0-3]| |)[0-9]))"), // 2020-5-1 or 2020-05-01
      new RegExp("("+now_year+'/((|0| )'+now_month+")/(([0-3]| |)[0-9]))"), // 2020/5/1 or 2020/05/01
      new RegExp("(((|0| )"+now_month+")/(([0-3]| |)[0-9])\\([월화수목금토일]\\))"),// 5/27(수) or 05/27(수)
      new RegExp("(((|0| )"+now_month+")\\.(([0-3]| |)[0-9])\\([월화수목금토일]\\))"),// 5.27(수) or 05.27(수)
      new RegExp("(판매일:(| )"+split_year+"-(( ||0)"+now_month+")-(([0-3]| |)[0-9]))"), //판매일: 20-07-14
      new RegExp("(주문일시:( |)"+now_year+"년 (( ||0)"+now_month+")월 (([0-3]| |)[0-9])일)")//(주문일시: 2020년 8월 8일)
    ];



    
    for(let i = 0, ob_len = Object.keys(object).length; i < ob_len; i++) {

      let date = object[i].date

      //빈 값 제거
      if(!date) {
        result = -1 
        break;
      }

       //value 날짜 포멧 체크
     for(let d = 0, len = regexp.length; d<len; d++) {

      //형식만
      let form_match = date.match(regexp_form[d]);
      //형식+날짜
      let all_match = date.match(regexp[d]);


      if( form_match !== null && all_match === null ) {
          // 1 형식만
        result = 1 
        break;
     } else if ( form_match !== null && all_match !== null) {
            // 2 모두 맞음
        result = 2
        break;
     }
    }
  }

  return result;

    
}

var checkJson = (state, json) => {
let  result = null
for(let v = 0, len = state.length; v < len; v++){
  let value = state[v]
  let price = value.price, date = value.date
  Object.keys(json).map(l => {
    let price_j = json[l].price, date_j = json[l].date
    if(price === price_j && date === date_j){
      result = -1
    }
  })

  if(result === -1){
   break;
  }
}
return result



}

export { alertMove, checkDate, checkJson }