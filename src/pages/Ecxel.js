
import React, { Component } from "react";
import Link from 'next/link';
import EcxelPop from '../Component/EcxelPop';

export async function getServerSideProps({query}) {
  if(typeof query.data === "undefined") {
    location.href = "/"
  }
  let month_data = query.data ? query.data : ""
  return {
    props: {month_data}, // will be passed to the page component as props
  }
}


class Ecxel extends Component {

  constructor(props) {
    super(props);
    this.state = {
        check: null,
        data: {},
        showing: {
          clicked: false,
          data:{}
        }
    };
  }
  
  changeState = (month_data, key, date) => {
    let data = month_data[key]

    this.setState  ({
      data: data,
      check: date
    })
 
  }

  showPop = (ob) => {
    let state = this.state;
    this.setState({
      ...state,
      showing: {
        clicked : !state.showing.clicked,
        data: ob
      }
    })
  }

  deletePop = () => {
    let state = this.state;

    this.setState({
      ...state,
      showing: {
        clicked : false,
        data: state.showing.data
      }
    })
  }


    render(){
      
  
      const max_style = {
        background: "white",
      margin: "0 auto" ,
      padding: "10px",
      maxWidth : "500px",
      height: "100%"
      }

      const button_style = {
        width: "2em",
        borderRadius: "100px"
      }

      const div_style = {
        marginBottom : "15px"
      }

      const td_style = {
        width: "20%",
        fontSize:" 0.8em"
      }

      const td_w_style = {
        width: "20%",
        textAlign : "center"
      }

      const table_style = {
        width: "100%"
      }

      const delete_pop = {
        float: "right",
        position: "relative",
        marginRight: "1.5rem",
        fontSize: "1rem"
      }

      

      let month_data = this.props.month_data;
      let state_data = this.state.data, state_len = Object.keys(state_data).length;

      //data info
      let data_len = Object.keys(state_data).length, sum = 0, all_array = {};

     
      for(let i= 0; i<data_len; i++) {

        let state_user = state_data[i].user, state_price = state_data[i].price;
        let state_error = state_data[i].error, state_checked_error = state_data[i].checked_error;// check_error은 수정시 사용
        let state_date = state_data[i].date, state_src = state_data[i].src;
        let state_evidence_src = state_data[i].evidence_src;

        //총합
        sum = sum + Number(state_price)
        //all_array에 유저 이름으로 키값 지정
        if(typeof all_array["user_"+state_user] === 'undefined') {

          all_array["user_"+state_user] = {
            price : null,
            error :  null,
            error_ob : [],
            src_ob: []
          }

        }
          //기존 값에 src추가
          all_array["user_"+state_user].src_ob[i] =   {
            src: state_src,
            date: state_date,
            price: state_price,
            error : state_error,
            evidence_src: state_evidence_src
          };
        
      }
      
        return(
          <div style={max_style}>
            <header> <Link href={{pathname:'/'}} ><a><input style={button_style} type="button" value ="<"/></a></Link></header>
            <br/>
            {
              Object.keys(month_data).map(v => {
              let year = v.split("_")[0],  month = v.split("_")[1].split(".")[0];
              let text = year+"년 "+month +"월"
              return <div key = {v} onClick={() => {this.changeState(month_data, v, text)}}>  {text}</div>;
             })
             } 
             <br/>
            {
            state_len === 0 ? 
            <div>날짜를 선택해주세요</div> :     <div>　<div>{this.state.check} </div><table border="1" style={table_style}>
                  <tbody>
                  <tr>
                    <th> </th>
                      <th>아영</th>
                      <th>정우</th>
                      <th>태영</th>
                      <th>총합</th>
                    </tr>
              
                    <tr>
                      <td style = {td_style}>엑셀 파일 </td>
                      <td style = {td_w_style}><button onClick={() => {this.showPop(all_array.user_way? all_array.user_way.src_ob : {})}}>버튼</button></td>
                      <td style = {td_w_style}><button onClick={() => {this.showPop(all_array.user_njw920? all_array.user_njw920.src_ob : {})}}>버튼</button></td>
                      <td style = {td_w_style}><button onClick={() => {this.showPop(all_array.user_tang? all_array.user_tang.src_ob : {})}}>버튼</button></td>
                    </tr>
                  </tbody>
                 
                
                  </table></div>
            
        }

  
  
        
        {this.state.showing.clicked ? <EcxelPop data={this.state.showing.data} /> : null}
        {this.state.showing.clicked ? <div style={delete_pop} onClick ={this.deletePop} >x</div> : null}
      
        </div>
        );
    }
}




export default Ecxel;
