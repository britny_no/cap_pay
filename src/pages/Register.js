import React, { Component } from "react";
import FormData from "form-data";
import axios from "axios";
import Slider from "react-slick";
import Link from 'next/link';

import * as js from '../js/index.js';
import  css from '../css/register.module.css';

export const getServerSideProps = async ({ req, res }) => {

  let login = req.session.login, id = req.session.login_id;
  let data = {
    id: id ? id : "",
    login: login ? login : ""
  };

  return {
    props: {
      register: {
        data: data ? data : ""
      }
    }
  }


}


class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
        data: {},
        json: {},
        check_value: 0
    };
  }
  
  componentDidMount () {
    let data = this.props.register.data;
    
    if(data.login !==1) {
      js.alertMove("로그인 해주세요", () => {
        location.href = "/"
      })
     
    } else{

      axios.get("/get_result")
      .then(
        res => {
         this.setState({
           json: res.data
         })
        }
      )
    }
  }


    
  previewImg = (event) => {
    event.preventDefault();

    //show loading
    let loading = document.getElementById("loading");
    loading.style.display = "block"
    
    let target = event.target;
    let files = target.files, len = files.length,  formData = new FormData(), url = "/pre_view";

    for(let i = 0;  i < len ; i++) {
        formData.append("upload"+i, files[i]); //uploadedData: img or video
    }

    axios
      .post(url, formData, {
        headers: {
          "Content-Type": `multipart/form-data;`,
        },
      })
      .then((res) => {
        //hide loading
        loading.style.display = "none"
        let response = res.data;
        //move error  -1;
        //type error -2;
        //api_error: -3, 1: success,
        if(response === -2) {
          alert("img(jpg, png, jpeg)형식을 지켜주세요")
        } else if(response === -2 || response === -3 ) {
          alert("다시 시도해주세요. 그래도 안되면 개발자에게 연락을...ㅎ")
        } else if(response === -4 ) {
          alert("이미지에 그림이 없습니다")
        }  else {
          let response_len = Object.keys( response).length
          let state_len = Object.keys( this.state.data).length, new_ob = {};
          let now_len = state_len === 0 ? 0 : state_len;
 
          for(let i = 0; i<response_len ; i++) {
            
            //price or date null, error = true
            response[i].error = !response[i].price || !response[i].date ;
            response[i].checked_error = false ;
            response[i].evidence_src = "";
            new_ob[i+now_len] = response[i]
          }

         
          this.setState({
            data:  {...this.state.data, ...new_ob}
          },() => {
            // console.log(this.state)
          })
        }
      })


    return;
  }

  addEvidenceSrc = (key, event) => { //key: 총합 순서, 

    let target = event.target;
    let files = target.files, len = files.length,  formData = new FormData(), url = "/pre_view";

    let loading = document.getElementById("loading");
    loading.style.display = "block"

    for(let i = 0;  i < len ; i++) {
      formData.append("upload"+i, files[i]); 
    }

  axios
    .post(url, formData, {
      headers: {
        "Content-Type": `multipart/form-data;`,
      },
    })
    .then((res) => {
      //hide loading
      loading.style.display = "none"
      let response = res.data;
      //move error  -1;
      //type error -2;
      //api_error: -3, 1: success,
      if(response === -2) {
        alert("img(jpg, png, jpeg)형식을 지켜주세요")
      } else if(response === -2 || response === -3 ) {
        alert("다시 시도해주세요. 그래도 안되면 개발자에게 연락을...ㅎ")
      }  else {
    
        let state = this.state.data
        let response_len = Object.keys( response).length;
        let evidence_src = "";

        //evidence_src 생성
        for(let i = 0; i<response_len ; i++) {
             evidence_src = evidence_src+ response[i].src + ", "
        }

        //state 변경
        let new_state = Object.keys(state).map(v => {
          if(v === key) {
            return {
              ... state[v],
              evidence_src : state[v].evidence_src + ", " + evidence_src 
            }
          } else {
            return state[v]
          }
        })


       
        this.setState({
          data:  new_state
        },() => {
          // console.log(this.state)
        })
      }
    })




  }

  revisePrice = (target, key, type) => {
    let val = target.value;
    let state = this.state.data;



    let new_ob = Object.keys(state).map(v => {
      let res ;
      if(v === key) {
        type === "date" ?  
        res = {
          ...state[v],
          date: val
        }  :
        res = {
          ...state[v],
          price: val
        }
       
      } else {
          res = state[v]
        }

        return res
    })
    
    this.setState({
      data: new_ob
     })
   

  }

 

  setError = (event) => {
    let tar = event.target, key = tar.value, state = this.state.data;
  
    let checked = tar.checked

    let new_ob = Object.keys(state).map(v => {
      let res ;
      if(v === key) {
        res = {
          ...state[v],
          checked_error: checked
        } 
      } else {
          res = state[v]
        }

        return res
    })
 


    this.setState({
      data: new_ob
     }, () => {
      //  console.log(this.state)
     })
  }


  deleteImg = (event) => {
    let tar = event.target, key = tar.placeholder, state = this.state.data, ob = [];

    //key 삭제
    delete state[key];

    Object.keys(state).map( v => {
      ob.push(state[v])
    })
 
    this.setState({
      data: ob
    }, () => {
    })

  }

  submit = (id) => {

    
  
    let data = this.state, state = data.data, json = data.json
    let loading = document.getElementById("loading");
 

    //날짜 포멧 체크
    let check_date = js.checkDate(state);
    let check_json = data.check_value === 0 ? js.checkJson(state, json) : 1

  
    if (Object.keys(state).length === 0) {
      alert("값을 넣어주세요")
    } else if ( check_date === -1) {
      alert("날짜를 적어주세요")
    }else if ( check_date === 0) {
      alert("2020 05 03포멧을 지켜주세요")
    } else if (check_date === 1) {
      alert("현재 달이 아닙니다")
    }else if (check_json === -1 && data.check_value ===0) {
      js.alertMove("올렸던 내용 아닌가요? 확인후 다시 올려 주세용", ()=> {
        this.setState({
          check_value: 1
        })
      })
    } else {
      let res = 1, url = "/register";
  
  
      for( let i = 0, len =  Object.keys(state).length; i<len; i++) {
        let price = state[i].price
        price = price.replace(/[^0-9]/, "")
    
        if(!state[i].date) {
          alert("빈값이 있습니다.")
          res = -1
          break;
        } else if(!price) {
          alert("빈값이 있습니다.")
          res = -1
          break;
        }
      }

    //show loading
    loading.style.display = "block"
      
      if(res !== -1) {
        data.user_id = id;
        axios
        .post(url, JSON.stringify(data), {
          headers: {
            "Content-Type": `application/json`,
          },
        })
        .then((res) => {
          loading.style.display = "none"
          js.alertMove("등록되셨습니다", () => {
            location.href = "/"
          })
        })
    
      }

    }


  }

  


    render(){

      const settings = {
        swipe: false,
        dots: true,
        dotsClass: "slick-dots slick-thumb",
        customPaging: function(i) {
          const style= {
            fontSize: "1em"
          }
         
          let dot;
          i === 1? dot = "증거" : dot =  "총합";
        return <a style={ style}>{dot}</a>;
        }
      };

      
      
      const evidence_settings = {
        dots: true,
        arrows: "false",
        centerMode: true,
        infinite: true,
        centerPadding: "0px",
        slidesToShow: 1,
        speed: 500,
        initialSlide: 0,
      };

      const max_style = {
        background: "white",
      margin: "0 auto" ,
      padding: "10px",
      maxWidth : "500px",
      height: "100%"
      }

      const img_style = {
        width: "100vw",
        maxHeight: "640px"
      }

    
      
      const input_style = {
       border :"1px solid black",
       borderRadius: "100px",
        marginLeft: "10px",
        paddingLeft: "10px",
        width: "60%",
        marginBottom: "10px"
      }
      
      const input_style_2 = {
        width: "1.5em",
        height: "3em",
        marginLeft: "10px",
        position: "relative",
        top: "18px",
      }

      const span_style = {
       float: "right"
      }

      const submit_style = {
        margin: "0 auto",
        display: "block",
        width: "4em",
        borderRadius: "100px",
        fontSize: "1.3em"
      }

      
      const delete_style = {
        margin: "0 auto",
        display: "block",
        width: "3.5em",
        borderRadius: "90px",
        fontSize: "1.1em",
        color: "#524848",
        background: "#ea000073",
        marginBottom: "20px"
      }

      

      const small_span_style = {
        fontSize: "0.5em"
      }
      const button_style = {
        width: "2em",
        borderRadius: "100px"
      }

      const load_style = {
        margin: "0 auto",
        display : "none",
        position: "fixed",
        width: "15em",
        /* right: 17vw; */
        top: "214px",
        left: "0",
        right: "0"
      
      }

      //login 접근 제한
      let src_data = this.state.data;

      let data = this.props.register.data;
      let id = data.id;




        return(
          <div style={max_style}>
             <header> <Link href={{pathname:'/'}} ><a><input style={button_style} type="button" value ="<"/></a></Link></header>
            {/* <form method ="post" onChange={this.previewImg}>
              <input type="file" name ="img" id="file" multiple />
            </form> */}
              {/* <span style={span_style}>총합: 1000원</span> */}

              {/* loading */}
              <img src="/loading.gif" style={load_style} id = "loading"/>
                {Object.keys(src_data).map(v => {
                  let price = src_data[v].price? src_data[v].price : ""
                  let date = src_data[v].date ? src_data[v].date : "";
                  let evidence_src = src_data[v].evidence_src.split(",") 
                  
                  return    <div className={css.map_div} key = {v}>
                              <Slider {...settings}>
                                        <div  key ={v}>
                                          <span className = {css.all_span_style}>{Number(v)+1}번째 총합</span>
                                          <img style={img_style} src={src_data[v].src}/>
                                          <br/>
                                          <span>가격</span>
                                          <input type ="number" placeholder="가격을 적어주세요" style = {input_style}  onChange={(event) => {this.revisePrice(event.target, v, "price" )}}  value={price} />
                                          <br/>
                                          <span>날짜</span>
                                          <input type ="text" placeholder="2020 05 03을 지켜주세요" style = {input_style} onChange={(event) => {this.revisePrice(event.target, v, "date" )}}   value={date}   />
                                          <br/>
                                          <span>에러 유무<span style={small_span_style}>(날짜, 가격 값이 사진과 다를시 체크)</span></span>
                                          <input type ="checkbox" defaultValue={v} style = {input_style_2} onChange={this.setError}  /> 
                                          {/*checked = {  error ? true: false} */}
                                          <br/>
                                          <br/>
                                          <input type ="button" value="삭제" placeholder={v} style = {delete_style} onClick={this.deleteImg}  />
                                        </div>

                                        {/* key값 넘겨주고 해당 키값의 evidence_src 추가 */}
                                      <div>
                                        <Slider {...evidence_settings}>
                                          {evidence_src.filter(k => k.replace(/ /gi, "") !== "").map(k => {
                          
                                            return <div>
                                                      <span className = {css.evidence_span_style}>{Number(v)+1}번째 총합 증거</span> 
                                                      <img className={css.evidence_img_style} src={k}/>
                                                    </div>;
                                          
                                          })}
                                           <div className = {css.div_slide}>
                                               {/* v는 총합의 키값이 되야한다 */}
                                               <label  className={css.label_style}  htmlFor={"evidence_src_"+v}>+증거 </label> <input onChange = {(event) => {this.addEvidenceSrc(v, event)}}  className = {css.initial_input} type="file" id={"evidence_src_"+v}   multiple/>
                                             </div>
                                        </Slider>
                                      </div>
                              </Slider>
                            </div>;
                })}

              {/* add img */}
              <div className = {css.div_slide}> 
                <label   htmlFor="ex_file">+</label> <input   className = {css.initial_input} type="file" id="ex_file" onChange={this.previewImg}  multiple/>
              </div>
           

           
        

            <br/>  <br/>  <br/>  <br/>  <br/>
            <input type="submit" style={submit_style} onClick = {() => {this.submit(id)}}/>
        </div>
        );
    }
}




export default Register;