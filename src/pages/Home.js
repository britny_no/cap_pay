
import React, { Component } from "react";
import axios from "axios";
import MovePage from "../Component/MovePage"



export const getServerSideProps = async ({ req, res }) => {


  let login = req.session.login, id = req.session.login_id;
  let data = {
    id: id ? id : "",
    login: login ? login : ""
  };

  return {
    props: {
      home: {
        data: data ? data : ""
      }
    }
  }


}



class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
        app_down: false
    };
  }



  componentDidMount() {
    if ('serviceWorker' in navigator) {
      this.setState({
        app_down :true
      })

      window.addEventListener('load', function() {
        navigator.serviceWorker.register('worker.js').then(function(registration) {
          console.log('Worker registration successful', registration.scope);

        }, function(err) {
          console.log('Worker registration failed', err);
        }).catch(function(err) {
          console.log(err);
        });
      });
    } else {

      console.log('Service Worker is not supported by browser.');
    }
    if(this.props.home.data.login === 1) {
      let deferredPrompt;
      window.addEventListener('beforeinstallprompt', event => {
    
        // Prevent Chrome 67 and earlier from automatically showing the prompt
        event.preventDefault();
    
        // Stash the event so it can be triggered later.
        deferredPrompt = event;
        
        // Attach the install prompt to a user gesture
        document.querySelector('#installBtn').addEventListener('click', event => {
          // Show the prompt
          deferredPrompt.prompt();
    
          // Wait for the user to respond to the prompt
          deferredPrompt.userChoice
            .then((choiceResult) => {
              if (choiceResult.outcome === 'accepted') {
                console.log('User accepted the A2HS prompt');
              } else {
                console.log('User dismissed the A2HS prompt');
              }
              deferredPrompt = null;
            });
        });
        
      
      });
    }
  
  }


  login = () => {
    let doc_forms = document.forms[0];
    let id = doc_forms.children[0].value, pw = doc_forms.children[2].value

    let url = "/login";
    let data = {
      id: id,
      pw: pw
    }


    axios
      .post(url, JSON.stringify(data), {
        headers: {
          "Content-Type": `application/json`,
          "Credentials": true
        },
      })
      .then((res) => {
        
        if (res.data === -1) {
          alert("아이디, 비번 틀림")
        } else {
          location.href="/"
        }

      })

  }
  render() {


    const max_style = {
      background: "white",
      margin: "0 auto",
      padding: "10px",
      maxWidth: "500px",
      paddingTop: "100px",
      paddingLeft: "5em",
      paddingRight: "7em",
      height: "100vh"

    }

    const input_style = {
      border: "1px solid #00000059",
      borderRadius: "100px",
      paddingLeft: " 10px",
      marginBottom: "10px",
      height: "3em"
    }

    const h_style = {
      paddingLeft: " 10px",
      marginBottom: "15px",
      fontSize: "1.5em"
    }

    const submit_style = {
      border: "1px solid #00000059",
      borderRadius: "100px",
      width: "3.5em",
      background: "white",
      height: "2em"

    }

    const button_style = {
      float: "right"
    }
    

    let data = this.props.home.data;
    let state = this.state;
    return (
      <div style={max_style}>
        <div>
          {data.login === 1 ? <div>
            <MovePage/>
            {state.app_down ?  <input type="button" style ={button_style} value="앱설치" id="installBtn"/> : null }
           
          </div>:
          <div>
            <h1 style={h_style}>로그인</h1>
            <form>
              <input type="text" style={input_style} placeholder="아이디" />
              <br/>
              <input type="password" style={input_style} placeholder="비밀번호" />
              <br />
              <input type="button" style={submit_style} value="로그인" onClick={this.login} />
            </form>
            </div>
              }
        </div>

      </div>
    );
  }
}




export default Home;
