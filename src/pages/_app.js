import React from 'react';
import App from 'next/app';

//all css
import "../../static/css/tailwind.css";
import "../../static/css/tailwind.src.css";




class MyApp extends App {
  // Only uncomment this method if you have blocking data requirements for
  // every single page in your application. This disables the ability to
  // perform automatic static optimization, causing every page in your app to
  // be server-side rendered.
  //
  // static async getInitialProps(appContext) {
  //   // calls page's `getInitialProps` and fills `appProps.pageProps`
  //   const appProps = await App.getInitialProps(appContext);
  //
  //   return { ...appProps }
  // }

  installPrompt = null;
// componentDidMount(){
//     console.log("Listening for Install prompt");
//     window.addEventListener('beforeinstallprompt',e=>{
//       // For older browsers
//       e.preventDefault();
//       console.log("Install Prompt fired");
//       this.installPrompt = e;
//       // See if the app is already installed, in that case, do nothing
//       if((window.matchMedia && window.matchMedia('(display-mode: standalone)').matches) || window.navigator.standalone === true){
//         return false;
//       }
//       // Set the state variable to make button visible
//       this.setState({
//         installButton:true
//       })
//     })
//   }
  render() {
    const { Component, pageProps } = this.props;
    return <Component {...pageProps} />;
  }
}

export default MyApp;