import Document, { Head, Main, NextScript } from 'next/document';
// Import styled components ServerStyleSheet
import { ServerStyleSheet } from 'styled-components';


export default class MyDocument extends Document {
//   static async getInitialProps(ctx) {
//     const sheet = new ServerStyleSheet();
//     const originalRenderPage = ctx.renderPage;

//     try {
//       ctx.renderPage = () => originalRenderPage({
//         enhanceApp: (App) => (props) => sheet.collectStyles(<App {...props} />),
//       });

//       const initialProps = await Document.getInitialProps(ctx);
//       return {
//         ...initialProps,
//         styles: (
//           <>
//             {initialProps.styles}
//             {sheet.getStyleElement()}
//           </>
//         ),
//       };
//     } finally {
//       sheet.seal();
//     }
//   }

  
  render() {
    // var next = document.getElementById("__next");
    // next.style.maxWidth = "500px";

    const bodyStyle = {
      background: "#e9e9e9",
    }
    return (
      <html>
        <Head>
          {/* Step 5: Output the styles in the head  */}
          <meta charSet='utf-8' />
          <meta name='viewport' content='initial-scale=1.0, width=device-width' />
          {this.props.styleTags}
          <link rel="stylesheet" type="text/css" charSet="UTF-8" href="/head_css/slick_min.css" />
          <link rel="stylesheet" type="text/css" href="/head_css/slick-theme_min.css" />
          <link rel="manifest" href="/manifest.json" />
          <meta name="theme-color" content="#FF9800"/>

      

        </Head>
        <body style={bodyStyle}>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
