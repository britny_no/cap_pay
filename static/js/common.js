const move = require('file-move');
const vision = require('@google-cloud/vision');
const {promisify} = require('util');
const fs = require('fs');
const path = require("path");

const client = new vision.ImageAnnotatorClient();
const readFile = promisify(fs.readFile);



var readJson = (files, dir_path) => {
  let  result = {};
  return new Promise(function(resolve, reject) {
    files.map(async (v) => { 
      let text =  await fs.readFileSync(dir_path+"/"+v, {encoding: 'utf8'});
      text = JSON.parse(text)
      result[v] = text
    })
    resolve(result)
  })
}


var moveFunction = (old_path, new_path) => {

    return new Promise(function(resolve, reject) {
        
        move(old_path, new_path,  (err) => {
            if(err)  {
            console.log(err)
            resolve(-1)
            } else {
            resolve(1)
            }
        })
       
      });
    

}

   // Make a call to the Vision API to detect text
   // params: inputFiles => array 
  function getTextFromFiles(inputFiles) {
    return new Promise( async function(resolve, reject) {
      
    // Read all of the given files and provide request objects that will be
    // passed to the Cloud Vision API in a batch request.
    const requests = await Promise.all(
      inputFiles.map(async filename => {
        const content = await readFile(filename);
        // console.log(` 👉 ${filename}`);
        return {
          image: {
            content: content.toString('base64'),
          },
          features: [{type: 'TEXT_DETECTION'}],
        };
      })
    );
  
    // Make a call to the Vision API to detect text
    const results = await client.batchAnnotateImages({requests});
    const detections = results[0].responses;
    let error = "1";
    await Promise.all(
      inputFiles.map(async (filename, i) => {
       
        const response = detections[i];
     
        if (response.error) { //response.error
          console.info(`API Error for ${filename}`, response.error);
          error = "-3"
          return ;
        }
        let result =  response.fullTextAnnotation.text.split("\n")

        let changed_name = filename.split("/pre_upload/")[1].split(".")[0]
        let res_path = path.normalize(__dirname+"/../../response/"+changed_name+"_r.json");

        if(error !== "1") {
          resolve(error)
        } else {
          responseToFile(res_path, JSON.stringify(result),(res) => {
            resolve(res)
          }) 
        }
      })
    );


    })
  }

  var responseToFile = (path,data, callback = () => {}) => {

    fs.writeFile(path,data,
    function(err){ 
      if(err) {
        callback("-1")
      } else{
        callback("1")
      }
  });


  }

  var findDatePrice = (array, v = []) => {
  
    let date = new Date(), now_year = date.getFullYear(), now_month = date.getMonth()+1, result_date = null, result_price = null,  regexp_key = null;
    let split_year = now_year.toString().substr(2,2)

  
    //날짜 정규표현식
    let regexp = [
      new RegExp('('+now_year+'\\.((([0-3]| |)[0-9])).((([0-3]| |)[0-9])))'), // 2020.05.01 or 2020.5.01
      new RegExp('('+now_year+' ((([0-3]| |)[0-9])) ((([0-3]| |)[0-9])))'), // 2020 05 01 or 2020 5 01
      new RegExp('('+now_year+'년,((([0-3]| |)[0-9])),(((([0-3]| |)[0-9])일)))'), // 2020년,5월,1일 or 2020년,05월,1일
      new RegExp('('+now_year+'년 ((([0-3]| |)[0-9]))월 (((([0-3]| |)[0-9])일)))'), // 2020년 5월 1일 or 2020년 05월 01일
      new RegExp('('+now_year+'년((([0-3]| |)[0-9]))월(((([0-3]| |)[0-9])일)))'), // 2020년5월1일 or 2020년05월1일
      new RegExp('('+now_year+'-((([0-3]| |)[0-9]))-((([0-3]| |)[0-9])))'), // 2020-5-1 or 2020-05-01
      new RegExp('('+now_year+'/((([0-3]| |)[0-9]))/((([0-3]| |)[0-9])))'), // 2020/5/1 or 2020/05/01
      new RegExp("(((([0-3]| |)[0-9]))/((([0-3]| |)[0-9]))\\([월화수목금토일]\\))"),// 5/27(수) or 05/27(수)
      new RegExp("(((([0-3]| |)[0-9]))\\.((([0-3]| |)[0-9]))\\([월화수목금토일]\\))"),// 5.27(수) or 05.27(수)
      new RegExp('(판매일:( |)'+split_year+'-((([0-3]| |)[0-9]))-((([0-3]| |)[0-9])))'), //판매일: 20-07-14
      new RegExp('(주문일시: '+now_year+'년 ((([0-3]| |)[0-9]))월 (((([0-3]| |)[0-9])일)))')//(주문일시: 2020년 8월 8일)
    ];

    
    let price_regexp = [
      //공백, 문자 제거
      new RegExp("(총결제금액)"), 
      new RegExp("(결제금액)"),  
      new RegExp("(합계)"),  
      new RegExp("(원\\*영)"),  
      new RegExp("(총 결제금액)"),  
      new RegExp("(계:)"), 
      new RegExp("(합계:)"), 
      new RegExp("(부가세:)"),  
      new RegExp("(총 상품가격)"), 
      new RegExp("(받을금액)"), 
      new RegExp("(총결제액)"),
      new RegExp("(합계 금액)"), 
      new RegExp("(일시불 \\/)"), 
 
    ];
   
  
    //모든 값에 유사 형식이 있는지 체크하고, date, price 모두 검출될 경우 break 사용
   for(let i = 0, array_len = Object.keys(array).length; i < array_len; i++ ){

   
  
      //value 날짜 포멧 체크
      for(let d = 0, len = regexp.length; d<len; d++) {
        let match = array[i].match(regexp[d]);

        if( match !== null ) {
          // regexp_key = d; // 정규표현식 키값 저장
          if(match[0].match(/(판매일:(| ))/gi) !==null){
            result_date = match[0].replace(/(판매일:| )/gi, "")
            result_date = result_date.split("-")
            result_date = "20"+result_date[0]+"-"+result_date[1]+"-"+result_date[2]
          } else {
            result_date = match[0]
          }
          break;
       }
      }


      //price 포멧 체크
      for(let p = 0, price_len = price_regexp.length; p < price_len; p++) {
  

        let check_price = array[i].match(price_regexp[p])

        if( check_price !== null ) {
          let reg =  new RegExp("([100-10000000000]|([100-10000000000]원))");
           //바로 안에 있을경우
           let value = array[i]
           let back_check =  value ?  value.replace(/[^0-9원]/g, "").match(reg) : null; 
           if(back_check !== null && value.match(/[-*]/gi) === null ) {
             result_price = back_check.input.replace(/[^0-9]/g, "");
             let re_len = result_price.length
             result_price.substr(re_len-1, 1) === "2" ? result_price = result_price.substr(0, re_len-1) : null;
             break;
           }

          //바로 뒤에 있을경우
          let value_2 = array[i+1]
          let back_check_2 =  value_2 ?  value_2.replace(/[^0-9원]/g, "").match(reg) : null; 
          if(back_check_2 !== null && value_2.match(/[-*]/gi) === null ) {
            result_price = back_check_2.input.replace(/[^0-9]/g, "");
            let re_len = result_price.length
             result_price.substr(re_len-1, 1) === "2" ? result_price = result_price.substr(0, re_len-1) : null;
            break;
          }

          //두번째 뒤에 있을경우
          let value_3 = array[i+2]
          let back_check_3 = value_3 ? value_3.replace(/[^0-9원]/g, "").match(reg) : null
          if(back_check_3 !== null && value_3.match(/[-*]/gi) === null   ) {
            result_price = back_check_3.input.replace(/[^0-9]/g, "");
            let re_len = result_price.length
             result_price.substr(re_len-1, 1) === "2" ? result_price = result_price.substr(0, re_len-1) : null;
            break;
          }
       }
      }

  
  
      if(result_date !== null && result_price !== null) {
        break;
      }
  
  
    }
   
    return [result_date, result_price]
    // return [res, day]
  
    
  } 

  var getDatePrice =  (array) => {
    return new Promise(async function(resolve, reject) {
      let result = {};

      for(let i = 0, len = array.length; i< len; i ++) {
        let file_name = array[i].split("pre_upload/")[1];
        let res_path = path.normalize(__dirname+"/../../response/"+file_name.split(".")[0]+"_r.json");
        let text =  await fs.readFileSync(res_path, {encoding: 'utf8'});
        text = JSON.parse(text)
  
        //check date
   
        let res =  await findDatePrice(text)
    
        result[i] = {
          src: "/pre_upload/"+file_name,
          date: res[0],
          price: res[1],
          error: false
        }
  
      }
  
      resolve(result);
  
     


    })
  

  } 


module.exports = {
    moveFunction, responseToFile, getTextFromFiles, findDatePrice, getDatePrice, readJson
}
