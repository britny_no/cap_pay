const express = require("express");
const next = require('next')




const dev = true //false => product, true => dev
const app = next({ dev })
const handle = app.getRequestHandler()

app 
.prepare()
.then(() => {
  const server = express()
const bodyParser = require("body-parser")
const formidable = require('formidable');
const fs = require('fs');
const cors = require('cors');

const js = require('./server_js');

const session = require('express-session')
var FileStore = require('session-file-store')(session);
 
var fileStoreOptions = {
  ttl: 1000000,
  reapInterval: 1000000 // delete sesison json file 
};
 

const {moveFunction, getTextFromFiles, findDatePrice, getDatePrice, readJson} = require('./static/js/common.js');

// Creates a client


server.use(session({
  store: new FileStore(fileStoreOptions),
  secret: Math.random().toString(36).substr(10,20), //when product, should random string
  resave: false,
  saveUninitialized: false,
  unset: "destroy",
  cookie: {  maxAge: 1000000 }
}))

server.use(bodyParser.json())

server.use(cors({
  credentials : true
}))

server.use('/pre_upload', express.static('pre_upload'));
server.use('/upload', express.static('upload'));
// Home
server.get("/", (req, res) => {
    const actualPage = '/Home'
    const queryParams = { }
    app.render(req, res, actualPage, queryParams)
})


// Month
server.get("/MonthAll", (req, res) => {
  let dir_path = __dirname+"/result";

  fs.readdir(dir_path, async (err, files) => {

    let result = await readJson(files, dir_path)

    const actualPage = '/MonthAll'
    const queryParams = { data: result }
  
    app.render(req, res, actualPage, queryParams)

  })

})

// Ecxel
server.get("/Ecxel", (req, res) => {
  let dir_path = __dirname+"/result";

  fs.readdir(dir_path, async (err, files) => {

    let result = await readJson(files, dir_path)

    const actualPage = '/Ecxel'
    const queryParams = { data: result }
  
    app.render(req, res, actualPage, queryParams)

  })

})




server.post("/login", (req, res) => {
  let body = req.body
  let id = body.id, pw = body.pw, result = "1"

  if(id === "way" && pw === "8888") {
    req.session.login = 1;
    req.session.login_id = "way";
  } else if (id === "tang" && pw === "8888") {
    req.session.login = 1;
    req.session.login_id = "tang";
  } else if (id === "njw920" && pw === "8888") {
    req.session.login = 1;
    req.session.login_id = "njw920";
  } else {
    result = "-1"
  }

  res.send(result)



})







//영수증 정보 저장
server.post("/register" , async (req, res) => {
  let date = new Date(), now_year = date.getFullYear(),  now_month = date.getMonth()+1;
  let data = req.body.data, id = req.body.user_id;

  let len = Object.keys(data).length;

    //json 파일 그달 json 가져와서 이어붙이기
    let result_path = __dirname+"/result/"+now_year+"_"+now_month+".json";
    //파일 없으면 생성 format: 2020_5.json
    if(!await fs.existsSync(result_path) ) {
      await fs.writeFileSync(result_path,"{}");
    }

      
    //기존 내용 가져오기
    let text =  await fs.readFileSync(result_path, {encoding: 'utf8'});
    text = JSON.parse(text);

    let old_length = Object.keys(text).length, new_ob = {};


  //이미지 이동
  for(let i = 0; i<len; i++) {
    let date = new Date(), now_year = date.getFullYear(),  now_month = date.getMonth()+1;
    let file_data = data[i]
    let old_path = __dirname+file_data.src; // format: /pre_upload/....
    let file_name = file_data.src.split("pre_upload/")[1];
    let dir_path =  __dirname+"/upload/"+now_year+"_"+now_month
    let new_path = dir_path+"/"+file_name;
    
  
    //날짜 파일 없으면 생성 format: 2020_5.json
    if(!await fs.existsSync(dir_path) ) {
      await fs.mkdirSync(dir_path)
    }
    //총합 사진이동
    let move_res = await moveFunction(old_path, new_path);

    if(move_res === -1) {
      console.log("error")
      res.send("-1")
      break;
    }

    //증거 사진 이동
    let evidence_src_array = file_data.evidence_src.split(",")
    let evidence_result = ""
    for(let v = 0, len = evidence_src_array.length; v< len; v++){
      let old_path = evidence_src_array[v].replace(/ /gi, "")
      if (old_path === ""){
        continue;
      }

      old_path = __dirname+old_path
      let evidence_name =old_path.split("pre_upload/")[1];
      let dir_path =  __dirname+"/upload/"+now_year+"_"+now_month+"/evidence"
      let new_path = dir_path+"/"+evidence_name;


    
      
      
      if(!await fs.existsSync(dir_path) ) {
        await fs.mkdirSync(dir_path)
      }

      let move_res = await moveFunction(old_path, new_path);

      if(move_res === -1) {
        console.log("error")
        res.send("-1")
        break;
      }

      evidence_result =  "/upload/"+now_year+"_"+now_month+"/evidence/"+evidence_name+","+evidence_result


    }



    //key 값 변경
    new_ob[i+old_length] = {
      src: "/upload/"+now_year+"_"+now_month+"/"+file_name,
      date: file_data.date,
      price: file_data.price,
      error: file_data.error,
      checked_error: file_data.checked_error,
      evidence_src : evidence_result,
      user: id
    }
    
  }

  //기존 ob에 새로운 데이터 넣기
  new_ob = JSON.stringify({...text, ...new_ob})
  fs.writeFile(result_path, new_ob, (err) => {
    if(err) {
      res.send("-1")
    } else {
      res.send("1")
    }
  });
})


server.post("/pre_view" , (req, res) => {
  let form = new formidable.IncomingForm();

  form.parse(req, async function (err, fields, files) {
    let date = new Date();
    let len = Object.keys(files).length;
    let src_ob = [], google_api = [];
    let result = "1";


    for(let i = 0; i<len; i++) {
      let file_data = files["upload"+i];
      let old_path = file_data.path;
      let file_name = file_data.name.split(".");
      let type =  file_data.type.split("/");
      let changed_name = file_name[0]+"_"+date.getTime()+"."+file_name[1]
      let new_path = __dirname+"/pre_upload/"+changed_name;

      // check type
      if(type[0] !== "image" &&  type[1] !== "png" && type[1] !== "jpg" && type[1] !== "jpeg") {
        result = "-2"; //type error
        break;
      }

      
       // check google api response file is exist
      if(!fs.existsSync( __dirname+"/response/"+changed_name.split(".")[0]+'_r.json') ) {

        google_api.push(new_path);
      }


      let move_res = await moveFunction(old_path, new_path);
      if( move_res !== -1) {
        src_ob.push(new_path)
      } else {
        result = "-1"; //move error
        break ;
      }
      
    }

    // 분석되지 못한 text google api 호출    
    result = google_api.length !== 0 ? await getTextFromFiles(google_api) : "1";


    // 이미지 분석 결과 json에 저장하고, json에서 값 축출
    if(result === "1") {
      // let insert = await getTextFromFiles(src_ob)
      src_ob = await getDatePrice(src_ob); // src_ob : text array
    }
   

  

    result = (result === "1" ? src_ob: result );
    res.send(result)
  })

})
  
server.post("/google",   (req, res) => {
  let form = new formidable.IncomingForm(), result = "1";
  
  form.parse(req, async function (err, fields, files) {
    let len = Object.keys(files).length,  src_ob = []

    for(let i = 0; i<len; i++) {
        let file_data = files["upload"+i];
        let old_path = file_data.path;
        let file_name = file_data.name;
        let type =  file_data.type.split("/");
        let new_path = __dirname+"/upload/"+file_name;

        // check type
        if(type[0] !== "image" &&  type[1] !== "png" && type[1] !== "jpg" && type[1] !== "jpeg") {
          result = "-1"; //type error
          break;
        }

        let response_f_name = file_name.split(".")[0];
         // check file exist
        if(await fs.existsSync( __dirname+"/response/"+response_f_name+'_r.json') ) {
          continue;
        }

    
        let move_res = await moveFunction(old_path, new_path);
      
        if( move_res !== -1) {
          src_ob.push(new_path)
        } else {
          result = "-1"; //move error
          break ;
        }
        
    }
   
    if(result !== "-1" && src_ob.length !==0) {
    
      getTextFromFiles(src_ob)
    }

    res.send(result);
  })
})

server.post("/delete_contents", async (req, res) => {
  let data = req.body, session = req.session
  let result = {}
  let changed_ob = {} , i = 0;

  if(session.login_id === "" || session.login !==1) {
   result = {
     state: 0,
     message: "로그인후 이용해주세요"
   }
  } else{
    let path = __dirname+"/result/"+data.file+".json";
    let text =  await fs.readFileSync(path, {encoding: 'utf8'});
    text = JSON.parse(text)
  
    Object.keys(text).map(v => {
      let value = text[v]
      if(!( data.src === value.src && data.price === value.price && data.date === value.date && session.login_id === value.user)) {
        changed_ob[i] = value
        i++
      }
    })


    fs.writeFile(path, JSON.stringify(changed_ob), null,  async (err) => {
      if(err) {  
        result = {
          state: -1,
          message: "error"
        }
      } else { 
        if(await fs.existsSync(__dirname+data.src) ) {
          fs.unlinkSync(__dirname+data.src);
        }
        result = {
        state: 1,
        message: "처리됐습니다"
      }
      }
    });

  }

  res.send(result)
})

// get_result
server.get("/get_result", async(req, res) => {
  let date = new Date(), now_year = date.getFullYear(), now_month = date.getMonth()+1
  let file = now_year.toString()+"_"+now_month.toString()+".json"

  let text =  await fs.readFileSync(__dirname+"/result/"+file, {encoding: 'utf8'});
  text = JSON.parse(text)
  res.send(text)

})


server.get("/debug", async (req, res) => {
  let result = js.compareData() //0: response, 1: upload img
  result = await js.showResult(result)
  res.send(result)
})





  


server.get('*', (req, res) => {
  return handle(req, res)
})


server.listen(process.env.PORT, err => {
    if (err) throw err
    console.log('> Ready on http://localhost:'+process.env.PORT)
  })

})